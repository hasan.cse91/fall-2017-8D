package com.hellohasan.fall2017_8d.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hellohasan.fall2017_8d.Student;

import java.util.ArrayList;
import java.util.List;

public class DatabaseQuery {

    private DatabaseHelper databaseHelper;

    public DatabaseQuery(Context context) {
        databaseHelper = DatabaseHelper.getInstance(context);
    }

    public void insertStudent(Student student){

        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put("name", student.getName());
        contentValues.put("phone", student.getPhone());

        sqLiteDatabase.insert("student", null, contentValues);
        sqLiteDatabase.close();
    }

    public List<Student> getStudentList(){
        SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        List<Student> studentList = new ArrayList<>();

        Cursor cursor = sqLiteDatabase.query("student", null, null, null,
                null, null, null);

        while (cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndex("_id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String phone = cursor.getString(cursor.getColumnIndex("phone"));

            Student student = new Student(id, name, phone);

            studentList.add(student);
        }

        cursor.close();
        sqLiteDatabase.close();

        return studentList;
    }
}
